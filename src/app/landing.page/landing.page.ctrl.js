(function () {
    'use strict';

    angular
        .module('webosmotic')
        .controller('LandingPageController', LandingPageController);

    /** @ngInject */
    function LandingPageController($scope,$interval) {

        var percent = 0, bar = angular.element('.transition-timer-carousel-progress-bar'), carousel = angular.element('#inSlider');

        $scope.fnProgressBarCarousel = function () {
            bar.css({width: percent + '%'});
            percent = percent + 0.5;
            if (percent > 100) {
                percent = 0;
                carousel.carousel('next');
            }
        };

        $scope.fnManageCarousel = function () {
            carousel.carousel({
                interval: false,
                pause: true
            }).on('slid.bs.carousel', function () {
                percent = 0;
            });
            var barInterval = $interval($scope.fnProgressBarCarousel, 30);
            carousel.hover(
                function () {
                    $interval.cancel(barInterval);
                },
                function () {
                    barInterval = $interval($scope.fnProgressBarCarousel, 30);
                });
        };

        $scope.fnNavClick = function(){
            // Page scrolling feature
            angular.element('a.page-scroll').bind('click', function (event) {
                var link = angular.element(this);
                angular.element('html, body').stop().animate({
                    scrollTop: angular.element(link.attr('href')).offset().top - 50
                }, 500);
                event.preventDefault();
            });
        };

        $scope.InitLandingPage = function () {
            angular.element('body').scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
            $scope.fnNavClick();
            $scope.fnManageCarousel();
            new WOW().init();
        };
    }
})();
