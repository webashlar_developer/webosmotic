(function () {
    'use strict';

    angular
        .module('webosmotic')
        .config(config);

    /** @ngInject */
    function config($logProvider, $locationProvider) {
        // Enable log
        $logProvider.debugEnabled(true);

        // use the HTML5 History API
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }

})();
