(function() {
  'use strict';

  angular
    .module('webosmotic', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap']);

})();
