(function() {
  'use strict';

  angular
    .module('webosmotic')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/landing.page/landing.page.html',
        controller: 'LandingPageController',
        controllerAs: 'LandingPage'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
